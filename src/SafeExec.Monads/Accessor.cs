using System;

namespace SafeExec.Monads
{
    public struct Accessor<TSrc, TVal>
    {
        private readonly TSrc _source;
        private readonly Func<TSrc, TVal> _func;

        public Accessor(TSrc source, Func<TSrc, TVal> func)
        {
            _source = source;
            _func = func;
        }

        public TVal Invoke() => 
            _func.Invoke(_source);

        public Either<Exception, TVal> SafeInvoke() => 
            _func.SafeInvoke(_source);

        public static implicit operator TVal(Accessor<TSrc, TVal> accessor) =>
            accessor.Invoke();

        public static implicit operator Func<TVal>(Accessor<TSrc, TVal> accessor) =>
            accessor.Invoke;
    }
}