using System.Collections.Generic;

namespace SafeExec.Monads
{
    public static partial class EnumerableExtensions
    {
        public static IEnumerable<TLeft> Lefts<TLeft, TRight>(
            this IEnumerable<Either<TLeft, TRight>> source) 
        {
            foreach (var either in source)
            {
                if(either.IsLeft) 
                {
                    yield return either.GetLeft().GetValue();
                }
            }
        }
        
        public static IEnumerable<TRight> Rights<TLeft, TRight>(
            this IEnumerable<Either<TLeft, TRight>> source) 
        {
            foreach (var either in source)
            {
                if(!either.IsLeft) 
                {
                    yield return either.GetRight().GetValue();
                }
            }
        }
    }
}