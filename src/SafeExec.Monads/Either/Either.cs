using System;

namespace SafeExec.Monads
{
    using static Option; 

    public readonly struct Either<TLeft, TRight>
    {
        private readonly TLeft _left;
        private readonly TRight _right;
        private readonly bool _isLeft;

        private Either(TLeft left, TRight right, bool isLeft)
        {
            _left = left;
            _right = right;
            _isLeft = isLeft;
        }

        public bool IsLeft => _isLeft;

        public bool IsRight => !_isLeft;

        public Option<TLeft> GetLeft() =>
            _isLeft ? Some(_left) : None<TLeft>();

        public Option<TRight> GetRight() =>
            _isLeft ? None<TRight>() : Some(_right);

        public Either<TMappedLeft, TMappedRight> Map<TMappedLeft, TMappedRight>(
            Func<TLeft, TMappedLeft> leftMapper, 
            Func<TRight, TMappedRight> rightMapper) =>
            _isLeft 
                ? Either<TMappedLeft, TMappedRight>.Left(leftMapper.Invoke(_left))
                : Either<TMappedLeft, TMappedRight>.Right(rightMapper.Invoke(_right));
        
        public Either<TMappedLeft, TRight> MapLeft<TMappedLeft>(
            Func<TLeft, TMappedLeft> leftMapper) =>
            _isLeft 
                ? Either<TMappedLeft, TRight>.Left(leftMapper.Invoke(_left))
                : Either<TMappedLeft, TRight>.Right(_right);
                
        public Either<TLeft, TMappedRight> MapRigtht<TMappedRight>(
            Func<TRight, TMappedRight> rightMapper) =>
            _isLeft 
                ? Either<TLeft, TMappedRight>.Left(_left)
                : Either<TLeft, TMappedRight>.Right(rightMapper.Invoke(_right));

        public Either<TMappedLeft, TMappedRight> Bind<TMappedLeft, TMappedRight>(
            Func<TLeft, Either<TMappedLeft, TMappedRight>> leftBinder, 
            Func<TRight, Either<TMappedLeft, TMappedRight>> rightBinder) => 
            _isLeft 
                ? leftBinder.Invoke(_left)
                : rightBinder.Invoke(_right);

        public Either<TMappedLeft, TRight> BindLeft<TMappedLeft>(
            Func<TLeft, Either<TMappedLeft, TRight>> leftBinder) =>
            _isLeft
                ? leftBinder.Invoke(_left)
                : Either<TMappedLeft, TRight>.Right(_right);

        public Either<TLeft, TMappedRight> BindRight<TMappedRight>(
            Func<TRight, Either<TLeft, TMappedRight>> rightBinder) =>
            _isLeft
                ? Either<TLeft, TMappedRight>.Left(_left)
                : rightBinder.Invoke(_right); 

        public static Either<TLeft, TRight> Left(TLeft left) => 
            new Either<TLeft, TRight>(left, default, true);

        public static Either<TLeft, TRight> Right(TRight right) => 
            new Either<TLeft, TRight>(default, right, false);

        public static implicit operator Either<TLeft, TRight>(TLeft left) =>
            Left(left);

        public static implicit operator Either<TLeft, TRight>(TRight right) =>
            Right(right);
    }

    public class Either
    {        
        public static Either<TLeft, TRight> Left<TLeft, TRight>(TLeft left) => 
            Either<TLeft, TRight>.Left(left);

        public static Either<TLeft, TRight> Right<TLeft, TRight>(TRight right) => 
            Either<TLeft, TRight>.Right(right);
    }
}