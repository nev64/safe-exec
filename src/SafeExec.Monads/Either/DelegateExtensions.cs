using System;

namespace SafeExec.Monads
{
    public static partial class DelegateExtensions
    {
        public static Either<Exception, TRes> SafeInvoke<TRes>(this Func<TRes> func) 
        {
            try
            {
                if (func == null)
                {
                    return new NullReferenceException();
                }   

                return func.Invoke();
            }
            catch(Exception e)
            {
                return e;
            }
        }

        
        public static Either<Exception, TRes> SafeInvoke<TArg, TRes>(
            this Func<TArg, TRes> func, 
            TArg arg) 
        {
            try
            {
                if (func == null)
                {
                    return new NullReferenceException();
                }   

                return func.Invoke(arg);
            }
            catch(Exception e)
            {
                return e;
            }
        }
        
        public static Either<Exception, Nothing> SafeInvoke(this Action action) 
        {
            try
            {
                if (action == null)
                {
                    return new NullReferenceException();
                }   
                
                action.Invoke();
                return Nothing.Value;
            }
            catch(Exception e)
            {
                return e;
            }
        }

        public static Either<Exception, Nothing> SafeInvoke<TArg1, TArg2>(
            this Action<TArg1, TArg2> action,
            TArg1 arg1,
            TArg2 arg2) 
        {
            try
            {
                if (action == null)
                {
                    return new NullReferenceException();
                }   
                
                action.Invoke(arg1, arg2);
                return Nothing.Value;
            }
            catch(Exception e)
            {
                return e;
            }
        }
    }
}