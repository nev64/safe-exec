namespace SafeExec.Monads
{
    public struct Nothing
    {
        public static Nothing Value => new Nothing();    
    }
}