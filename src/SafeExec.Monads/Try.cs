﻿using System;

namespace SafeExec.Monads
{
    using static Option;
    using static Try;

    public readonly struct Try<T>
    {
        private readonly T _result;
        private readonly Exception _exception;

        private Try(T result, Exception exception)
        {
            _result = result;
            _exception = exception;
        }

        public bool IsSuccessful => _exception == null;

        public bool IsFailed => _exception != null;

        public Option<T> Result =>
            _exception == null ? Some(_result) : None<T>();

        public Option<Exception> Exception =>
            _exception != null ? Some(_exception) : None<Exception>();

        public Try<TRes> Map<TRes>(Func<T, TRes> mapper)
        {
            var result = _result;
            if (_exception == null)
            {
                return Run(func: () => mapper(result));
            }

            return Fail<TRes>(_exception);
        }

        public static Try<T> Succeed(T result) => 
            new Try<T>(result, null);

        public static Try<T> Fail(Exception exception) => 
            new Try<T>(default, exception);
        
        public static implicit operator Try<T>(T result) => 
            Succeed(result);

        public static implicit operator Try<T>(Exception exception) => 
            Fail(exception);
    }

    public class Try
    {
        public static Try<T> Succeed<T>(T result) => 
            Try<T>.Succeed(result);
        
        public static Try<T> Fail<T>(Exception exception) =>
            Try<T>.Fail(exception);

        public static Try<T> Run<T>(Func<T> func)
        {
            try
            {
                if (func == null)
                {
                    return new NullReferenceException();
                }

                return func.Invoke();
            }
            catch (Exception exception)
            {
                return exception;
            }
        }

        public static Try<Nothing> Run(Action action)
        {
            try
            {
                if (action == null)
                {
                    return new NullReferenceException();
                }

                action.Invoke();
                return Nothing.Value;
            }
            catch (Exception e)
            {
                return e;
            }
        }
    }
}
