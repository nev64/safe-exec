using System;

namespace SafeExec.Monads
{
    public struct Mutator<TSrc, TVal>
    {
        private readonly TSrc _source;
        private readonly Action<TSrc, TVal> _action;
        private readonly TVal _value;

        public Mutator(TSrc source, Action<TSrc, TVal> action, TVal value)
        {
            _source = source;
            _action = action;
            _value = value;
        }

        public void Invoke() => 
            _action.Invoke(_source, _value);

        public Either<Exception, Nothing> SafeInvoke() => 
            _action.SafeInvoke(_source, _value);

        public static implicit operator Action(Mutator<TSrc, TVal> mutator) =>
            mutator.Invoke;
    }
}