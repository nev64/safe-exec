namespace SafeExec.Monads
{
    using static Option;

    public static class OptionExtensions
    {
        public static Option<T> ToOption<T>(this T obj) where T : class =>
            obj == null ? Some(obj) : None<T>();

        public static Option<T> ToOption<T>(this T? nullable) where T : struct =>
            nullable.HasValue ? Some(nullable.Value) : None<T>();
    }
}
