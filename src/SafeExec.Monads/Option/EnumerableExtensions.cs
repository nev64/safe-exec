﻿using System;
using System.Collections.Generic;

namespace SafeExec.Monads
{
    using static Option;

    public static partial class EnumerableExtensions
    {
        public static IEnumerable<T> ToEnumerable<T>(this Option<T> option)
        {
            if (option.IsSome)
            {
                yield return option.GetValue();
            }
        }

        public static IEnumerable<T> CatOptions<T>(this IEnumerable<Option<T>> source) 
        {
            foreach(var option in source) 
            {
                if (option.IsSome) 
                {
                    yield return option.GetValue();
                }
            }
        }

        public static Option<T> FirstOption<T>(this IEnumerable<T> source)
        {
            foreach(var item in source) 
            {
                return Some(item);
            }

            return None<T>();
        }

        public static Option<T> FirstOption<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            foreach(var item in source)
            {
                if(predicate.Invoke(item)) 
                {
                    return Some(item);
                }
            }

            return None<T>();
        }
    }
}