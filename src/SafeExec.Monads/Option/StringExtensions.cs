using System;
using System.Globalization;

namespace SafeExec.Monads 
{
    using static Option;

    public static partial class StringExtensions 
    {
        public static Option<byte> ParseByte(
            this string value,
            NumberStyles style = default,
            IFormatProvider provider = default) =>
            byte.TryParse(value, style, provider, out var result)
                ? Some(result)
                : None<byte>();

        public static Option<int> ParseInt(
            this string value, 
            NumberStyles style = default, 
            IFormatProvider provider = default) =>        
            int.TryParse(value, style, provider, out var result)
                ? Some(result)
                : None<int>();
        

        public static Option<long> ParseLong(
            this string value,
            NumberStyles style = default,
            IFormatProvider provider = default) =>
            long.TryParse(value, style, provider, out var result)
                ? Some(result)
                : None<long>();
        
        public static Option<DateTimeOffset> ParseDate(
            this string value,
            DateTimeStyles style = default,
            IFormatProvider provider = default) =>
            DateTimeOffset.TryParse(value, provider, style, out var result)
                ? Some(result)
                : None<DateTimeOffset>();
        
        public static Option<TimeSpan> ParseTimeSpan(this string value) =>
            TimeSpan.TryParse(value, out var result)
                ? Some(result)
                : None<TimeSpan>();
    }
}