﻿using System;
using System.Collections;

namespace SafeExec.Monads
{
    public readonly struct Option<T>
    {
        private readonly T _value;
        private readonly bool _isSome;

        public Option(T value, bool isSome)
        {
            _value = value;
            _isSome = isSome;
        }

        public bool IsSome => _isSome;

        public bool IsNone => _isSome;

        public T GetValue() => 
            _isSome ? _value : throw new NoneOptionException();

        public T GetValue(T defaultValue) => 
            _isSome ? _value : defaultValue;

        public T GetValue(Func<T> defaultValueFactory) => 
            _isSome ? _value : defaultValueFactory.Invoke();

        public Option<TRes> Map<TRes>(Func<T, TRes> mapper) => 
            _isSome ? Option<TRes>.Some(mapper.Invoke(_value)) : Option<TRes>.None();

        public Option<TRes> Bind<TRes>(Func<T, Option<TRes>> mapper) =>
            _isSome ? mapper.Invoke(_value) : Option<TRes>.None();

        public Option<T> Filter(Func<T, bool> predicate) => 
            _isSome 
                ? (predicate.Invoke(_value) ? this : None())
                : None();

        public override bool Equals(object obj)
        {
            if (obj is Option<T> option)
            {
                return option._isSome == _isSome &&
                       option._value == _value;
            }

            return false;
        }

        public static Option<T> Some(T value) => new Option<T>(value, true);

        public static Option<T> None() => new Option<T>(default, false);
    }

    public static class Option 
    {
        public static Option<T> Some<T>(T value) => Option<T>.Some(value);
        
        public static Option<T> None<T>() => Option<T>.None();

        public static Option<T> Return<T>(T value) => new Option<T>(value, true);
    }
}
