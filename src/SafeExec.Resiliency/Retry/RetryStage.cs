﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SafeExec.Monads;

namespace SafeExec.Resiliency
{
    using static Try;

    public class RetryStage<TReq, TRes> : IIntermediateStage<TReq, TRes>
    {
        private readonly IEnumerable<TimeSpan> _delaysGenerator;

        public RetryStage(IEnumerable<TimeSpan> delaysGenerator)
        {
            _delaysGenerator = delaysGenerator;
        }

        public Try<TRes> Invoke(TReq request, ITailStage<TReq, TRes> tail)
        {
            using (var delays = _delaysGenerator.GetEnumerator())
            {
                Try<TRes> @try;
                var shouldRetry = false;
                do
                {
                    @try = tail.Invoke(request);
                    
                    if (@try.IsFailed)
                    {
                        Task.Delay(delays.Current).Wait();
                        shouldRetry = !delays.MoveNext();
                        if (!shouldRetry)
                        {
                            @try = Fail<TRes>(new RetryException());
                        }
                    }
                } while (shouldRetry);

                return @try; 
            }
        }

        public async Task<Try<TRes>> InvokeAsync(TReq request, ITailStage<TReq, TRes> tail)
        {
            using (var delays = _delaysGenerator.GetEnumerator())
            {
                Try<TRes> @try;
                var shouldRetry = false;
                do
                {
                    @try = tail.Invoke(request);

                    if (@try.IsFailed)
                    {
                        await Task.Delay(delays.Current);
                        shouldRetry = !delays.MoveNext();
                        if (!shouldRetry)
                        {
                            @try = Fail<TRes>(new RetryException());
                        }
                    }
                } while (shouldRetry);

                return @try;
            }
        }
    }
}
