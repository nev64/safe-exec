﻿using System;
using System.Linq;
using SafeExec.Monads;

namespace SafeExec.Resiliency
{
    public class PipelineAttribute : Attribute
    {
        public PipelineAttribute(params Type[] stages)
        {
            if (stages.Length == 0)
            {
                throw new Exception("At least one stage must be declared.");
            }

            var incorrentStage = stages.FirstOption(_ => _.GetInterfaces().Any(typeof(IIntermediateStage<,>).Equals));
            if (incorrentStage.IsSome)
            {
                throw new Exception(
                    $"Class {incorrentStage.Map(_ => _.Name).GetValue()} don't implement IStage interface.");
            }

            Stages = stages;
        }

        public Type[] Stages { get; }
    }
}
