﻿using System;
using System.Threading.Tasks;
using SafeExec.Monads;

namespace SafeExec.Resiliency
{
    public interface IIntermediateStage<TReq, TRes>
    {
        Try<TRes> Invoke(TReq request, ITailStage<TReq, TRes> tail);
        Task<Try<TRes>> InvokeAsync(TReq request, ITailStage<TReq, TRes> tail);
    }
}