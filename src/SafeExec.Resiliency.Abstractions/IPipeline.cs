﻿using System.Threading.Tasks;

namespace SafeExec.Resiliency
{
    public interface IPipeline
    {
        TRes Invoke<TReq, TRes>(TReq request);
        Task<TRes> InvokeAsync<TReq, TRes>(TReq request);
    }
}
