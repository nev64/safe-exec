﻿using System.Threading.Tasks;

namespace SafeExec.Resiliency
{
    public interface IHeadStage<in TReq, TRes>
    {
        TRes Invoke(TReq request);
        Task<TRes> InvokeAsync(TReq request);
    }
}