﻿using System.Threading.Tasks;
using SafeExec.Monads;

namespace SafeExec.Resiliency
{
    public interface ITransformStage<in TReq, TRes, TTailReq, TTailRes>
    {
        Try<TRes> Invoke(TReq request, ITailStage<TTailReq, TTailRes> tail);
        Task<Try<TRes>> InvokeAsync(TReq request, ITailStage<TTailReq, TTailRes> tail);
    }
}