﻿using System.Threading.Tasks;
using SafeExec.Monads;

namespace SafeExec.Resiliency
{
    public interface ITailStage<in TReq, TRes>
    {
        Try<TRes> Invoke(TReq request);
        Task<Try<TRes>> InvokeAsync(TReq request);
    }
}
