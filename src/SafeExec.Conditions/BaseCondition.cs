namespace SafeExec.Conditions
{
    public abstract class BaseCondition : ICondition
    {
        protected BaseCondition(string complianceMessage, string violationMessage)
        {
            ComplianceMessage = complianceMessage;
            ViolationMessage = violationMessage;
        }

        public string ComplianceMessage { get; }

        public string ViolationMessage { get; }

        public abstract bool IsMet();

        public bool IsViolated() => !IsMet();

        public Check Check() =>
            Conditions.Check.Of(IsMet(), ComplianceMessage, ViolationMessage);
    }
}