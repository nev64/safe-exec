﻿using SafeExec.Monads;

namespace SafeExec.Conditions
{
    public struct Check
    {
        private readonly bool _state;
        private readonly string _message;

        private Check(bool state, string message)
        {
            _state = state;
            _message = message;
        }

        public bool State => _state;

        public string Message => _message;

        public Either<string, string> ToEither() =>
            _state ? Either<string, string>.Right(_message) : Either<string, string>.Left(_message);

        public static Check Of(bool state, string trueMessage, string falseMessage) =>
            new Check(state, state ? trueMessage : falseMessage);
    }
}
