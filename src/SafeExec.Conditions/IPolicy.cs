﻿namespace SafeExec.Conditions
{
    public interface ICondition
    {
        bool IsMet();
        bool IsViolated();
        Check Check();
    }
}
