using System.Diagnostics;
using Xunit;
using Xunit.Abstractions;

namespace SafeExec.Conditions.Tests
{
    public class StructPolicyTests
    {
        private const string COMPLIANCE_MESSAGE = "Test Policy is complied.";
        private const string VIOLATION_MESSAGE = "Test Policy is violated.";

        private readonly ITestOutputHelper _helper;

        public StructPolicyTests(ITestOutputHelper helper)
        {
            _helper = helper;
        }

        private ICondition GetPolicy(bool result) => new TestCondition(result);

        [Fact]
        public void TestIsMet()
        {
            var policy = GetPolicy(true);
            Assert.True(policy.IsMet());
        }

        [Fact]
        public void TestIsViolated()
        {
            var policy = GetPolicy(true);
            Assert.False(policy.IsViolated());
        }

        [Fact]
        public void TestComplianceMessage()
        {
            var message = GetPolicy(true)
                .Check()
                .Message;

            Assert.Equal(COMPLIANCE_MESSAGE, message);
        }

        private class TestCondition : ICondition
        {
            private readonly bool _result;

            public TestCondition(bool result)
            {
                _result = result;
            }

            public bool IsMet() => _result;

            public bool IsViolated() => !_result;

            public Check Check() =>
                Conditions.Check.Of(_result, COMPLIANCE_MESSAGE, VIOLATION_MESSAGE);
        }
    }
}
