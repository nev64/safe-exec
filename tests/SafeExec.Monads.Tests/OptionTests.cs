﻿using System;
using Xunit;
using static SafeExec.Monads.Option;

namespace SafeExec.Monads.Tests
{
    public class OptionTests
    {
        // return a >>= f ≡ f a
        [Fact] 
        public void FirstMonadicLaw()
        {
            var a = 5;
            Func<int, Option<int>> f = _ => Some(_ * 4);

            var result = Return(a).Bind(f);
            
            Assert.True(result.IsSome);
            Assert.Equal(20, result.GetValue());
        }

        // m >>= return ≡ m
        [Fact]
        public void SecondMonadicLaw()
        {
            var m = Some(20);
            var result = m.Bind(Return);

            Assert.True(result.IsSome);
            Assert.Equal(20, result.GetValue());
        }

        [Fact]
        public void ThirdMonadicLaw()
        {
            var m = Some(2);
            Func<int, Option<int>> f = _ => Some(_ * 2);
            Func<int, Option<int>> g = _ => Some(_ * 5);
            var result1 = m.Bind(f).Bind(g);
            var result2 = m.Bind(x => f(x).Bind(g));


            Assert.True(result1.IsSome);
            Assert.Equal(20, result1.GetValue());

            Assert.True(result2.IsSome);
            Assert.Equal(20, result2.GetValue());
        }
    }
}
