# SafeExec

SafeExec consists of several libraries: Monads, Conditions and Resiliency. These libraries define several structures and classes that allow to write safer code.

## Monads

SafeExec.Monads library contains custom structure-based implementation of some well-know monads often used in functional languages and extension methods for common classes that allow to use monads in easier way. Currently implemented:
* Option (Maybe, Optional) - defines a monad that may contain value or may not;
* Either - monad that contains one of two values: Left or Right;
* Try - encapsulates result of delegate invocation and can contain either a result (`SafeExec.Monads.Nothing` in case of `System.Action`) or an exception.

## Condition

This library contains base building blocks to describe business rules or general code conditions as a composition of objects that can be evaluated on demand. Condition contains methods that evaluate defined rule against incoming values and return resolution as a combination of boolean value and message. Also it allows to execute specific branches of code against the resolution in functional manner.

## Resiliency

SafeExec contains a set of libraries that define implementation of Resiliency Patterns by using Monads and Conditions models. Resiliency implements processing pipeline that consists of a number of ordered stages, and pipeline configuration model as well. Currently implemented:
* Retry - invokes next stage several times if the first call failed.
